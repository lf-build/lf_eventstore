﻿using LendFoundry.Foundation.Services;
using System;
using System.Threading.Tasks;
#if DOTNET2
using Microsoft.AspNetCore.Mvc;
#else
using Microsoft.AspNet.Mvc;
#endif
namespace LendFoundry.EventStore.Api.Controllers
{
    /// <summary>
    /// EventStoreController class
    /// </summary>
    /// <seealso cref="LendFoundry.Foundation.Services.ExtendedController" />
    [Route("/")]
    public class EventStoreController : ExtendedController
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="EventStoreController"/> class.
        /// </summary>
        /// <param name="eventStoreService">The event store service.</param>
        /// <exception cref="ArgumentException">eventStoreService</exception>
        public EventStoreController(IEventStoreService eventStoreService)
        {
            if (eventStoreService == null)
                throw new ArgumentException($"{nameof(eventStoreService)} is mandatory");

            EventStoreService = eventStoreService;           
        }

        private IEventStoreService EventStoreService { get; }      

        private static NoContentResult NoContentResult { get; } = new NoContentResult();

        /// <summary>
        /// Gets the data by identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        [HttpGet("{id}")]
        [ProducesResponseType(typeof(IEventData[]), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> GetDataByIdAsync(string id)
        {
            try
            {
                return Ok(await EventStoreService.GetDataById(id));
            }
            catch (Exception ex)
            {
                return ErrorResult.BadRequest(ex.Message);
            }
        }
    }
}