﻿using LendFoundry.EventHub;
using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Listener;
using LendFoundry.Security.Tokens;
using LendFoundry.Tenant.Client;
using System;
using System.Collections.Generic;
using LendFoundry.Configuration;

namespace LendFoundry.EventStore
{
    public class EventStoreListener : ListenerBase, IEventStoreListener
    {
        public EventStoreListener
         (
             IEventStoreRepositoryFactory repositoryFactory,
             ITokenHandler tokenHandler,
             ILoggerFactory loggerFactory,
             IEventHubClientFactory eventHubFactory,
             ITenantServiceFactory tenantServiceFactory
         ) : base(tokenHandler, eventHubFactory, loggerFactory, tenantServiceFactory, Settings.ServiceName)
        {
            if (loggerFactory == null) throw new ArgumentException($"{nameof(loggerFactory)} is mandatory");

            if (repositoryFactory == null) throw new ArgumentException($"{nameof(repositoryFactory)} is mandatory");

            if (tokenHandler == null) throw new ArgumentException($"{nameof(tokenHandler)} is mandatory");

            if (tenantServiceFactory == null) throw new ArgumentException($"{nameof(tenantServiceFactory)} is mandatory");

            EventStoreRepositoryFactory = repositoryFactory;
            TokenHandler = tokenHandler;
            LoggerFactory = loggerFactory;
            EventHubFactory = eventHubFactory;
            TenantServiceFactory = tenantServiceFactory;
            Logger = LoggerFactory.CreateLogger();
        }

        private ILogger Logger { get; }
        private IEventStoreRepositoryFactory EventStoreRepositoryFactory { get; }
        private ITokenHandler TokenHandler { get; }
        public ILoggerFactory LoggerFactory { get; }
        public IEventHubClientFactory EventHubFactory { get; }
        public ITenantServiceFactory TenantServiceFactory { get; }
        private string eventStoreEventName = "all";

        public override List<string> GetEventNames(string tenant)
        {
            return new List<string>() { eventStoreEventName };
        }

        public override List<string> SubscribeEvents(string tenant, ILogger logger)
        {
            var serviceName = Settings.ServiceName;
            var token = TokenHandler.Issue(tenant, serviceName);
            var reader = new StaticTokenReader(token.Value);
            var eventHub = EventHubFactory.Create(reader);
            var repository = EventStoreRepositoryFactory.Create(reader);
            var eventHubs = new List<IEventHubClient>();
            var uniqueEvents = new List<string>();

            Logger.Info($"It was made subscription to EventHub with the Event: #all for tenant {tenant}");

            uniqueEvents.Add(eventStoreEventName);

            eventHub.On(eventStoreEventName, e =>
            {
                try
                {
                    Logger.Info($"Received event #{e.Name} - {e.Id} from #{e.TenantId}");
                    var eventToStore = new EventData
                    {
                        TenantId = e.TenantId,
                        Name = e.Name,
                        Source = e.Source,
                        Time = e.Time,
                        Username = e.UserName,
                        IpAddress = e.IpAddress
                    };
                    if (e.Data != null)
                        eventToStore.Data = e.Data;
                    else
                        Logger.Warn($"Event does not have [Data] information", e);


                    if (string.IsNullOrWhiteSpace(eventToStore.TenantId))
                    {
                        Logger.Error($"#{nameof(eventToStore.TenantId)} cannot be null, save process cancelled", null, e);
                        return;
                    }

                    repository.Add(eventToStore);

                    Logger.Info($"Event #{eventToStore.Name} from tenant #{eventToStore.TenantId} stored successfully ", eventToStore);

                    Logger.Info("Waiting for events...");
                }
                catch (Exception ex)
                {
                    Logger.Error($"Unhandled exception while saving event", ex, e);
                }
            });

            eventHubs.Add(eventHub);

            logger.Info($"It was made subscription to EventHub with the Event: #all for {tenant}");

            return uniqueEvents;
        }
    }
}