﻿using LendFoundry.EventHub.Client;
using LendFoundry.Foundation.Logging;
using LendFoundry.Security.Tokens;
using LendFoundry.Tenant.Client;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace LendFoundry.EventStore
{
    public class EventStoreService : IEventStoreService
    {
        public EventStoreService(
            IEventStoreRepository eventStoreRepository)
        {
            EventStoreRepository = eventStoreRepository;
        }
        private IEventStoreRepository EventStoreRepository { get; }

        public async Task<List<IEventData>> GetDataById(string id)
        {
            if (string.IsNullOrEmpty(id))
                throw new ArgumentException($"Argument can not be Null {id}");
            return await EventStoreRepository.GetDataById(id);
        }
    }
}