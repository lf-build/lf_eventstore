﻿#if DOTNET2
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;
#else
using Microsoft.AspNet.Builder;
using Microsoft.Framework.DependencyInjection;
#endif
namespace LendFoundry.EventStore
{
    public static class EventStoreListenerExtensions
    {
        public static void UseEventStoreListener(this IApplicationBuilder application)
        {
            application.ApplicationServices.GetRequiredService<IEventStoreListener>().Start();
        }
    }
}
