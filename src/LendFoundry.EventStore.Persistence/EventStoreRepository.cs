﻿using LendFoundry.Foundation.Persistence.Mongo;
using LendFoundry.Security.Encryption;
using LendFoundry.Tenant.Client;
using MongoDB.Bson.Serialization;
using MongoDB.Driver;
using MongoDB.Driver.Linq;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace LendFoundry.EventStore.Persistence
{
    public class EventStoreRepository : MongoRepository<IEventData, EventData>, IEventStoreRepository
    {
        static EventStoreRepository()
        {

        }

        public EventStoreRepository(ITenantService tenantService, IMongoConfiguration configuration, IEncryptionService encrypterService) :
                base(tenantService, configuration, "event-store")
        {
            if (!BsonClassMap.IsClassMapRegistered(typeof(EventData)))
            {
                BsonClassMap.RegisterClassMap<EventData>(map =>
            {
                map.AutoMap();
                var type = typeof(EventData);
                map.MapMember(m => m.Data).SetSerializer(new BsonEncryptor<object, object>(encrypterService));
                map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
                map.SetIsRootClass(true);
            });
            }
            CreateIndexIfNotExists
               (
                  indexName: "tenant-id",
                  index: Builders<IEventData>.IndexKeys.Ascending(i => i.TenantId)
               );
            CreateIndexIfNotExists
               (
                  indexName: "tenant-name-id",
                  index: Builders<IEventData>.IndexKeys.Ascending(i => i.TenantId).Ascending(i => i.Name)
               );
        }

        public async Task<List<IEventData>> GetDataById(string id)
        {
            return await Query.Where(a => a.Id == id).ToListAsync();
        }
    }
}