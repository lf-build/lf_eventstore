﻿using LendFoundry.Security.Tokens;

namespace LendFoundry.EventStore
{
    public interface IEventStoreRepositoryFactory
    {
        IEventStoreRepository Create(ITokenReader reader);
    }
}