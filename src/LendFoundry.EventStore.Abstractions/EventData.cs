﻿using System;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Persistence;

namespace LendFoundry.EventStore
{
    public class EventData : Aggregate, IEventData
    {
        public string Name { get; set; }
        public TimeBucket Time { get; set; }
        public string Source { get; set; }        
        public object Data { get; set; }
        public string Username { get; set; }
        public string IpAddress { get; set; }
    }
}