﻿using LendFoundry.Foundation.Client;
using System.Collections.Generic;

namespace LendFoundry.EventStore
{
    public class EventStoreConfigurations : IDependencyConfiguration
    {
        public Dictionary<string, string> Dependencies { get; set; }
        public string Database { get; set; }
        public string ConnectionString { get; set; }
    }
}
