﻿using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Persistence;

namespace LendFoundry.EventStore
{
    public interface IEventData : IAggregate
    {     
        string Name { get; set; }
        TimeBucket Time { get; set; }
        string Source { get; set; }
        object Data { get; set; }
        string Username { get; set; }
        string IpAddress { get; set; }
    }
}