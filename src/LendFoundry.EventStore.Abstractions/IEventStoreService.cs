﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace LendFoundry.EventStore
{
    public interface IEventStoreService
    {
        Task<List<IEventData>> GetDataById(string eventName);
    }
}