﻿namespace LendFoundry.EventStore
{
    public interface IEventStoreListener
    {
        void Start();
    }
}