﻿using LendFoundry.Foundation.Persistence;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace LendFoundry.EventStore
{
    public interface IEventStoreRepository :  IRepository<IEventData>
    {
        Task<List<IEventData>> GetDataById(string id);
    }
}