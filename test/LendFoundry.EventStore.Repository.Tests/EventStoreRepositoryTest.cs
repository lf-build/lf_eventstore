﻿using LendFoundry.EventStore.Persistence;
using LendFoundry.Foundation.Persistence.Mongo;
using LendFoundry.Foundation.Testing.Mongo;
using LendFoundry.Tenant.Client;
using Moq;
using System;
using System.Linq;
using Xunit;

namespace LendFoundry.EventStore.Repository.Tests
{
    public class EventStoreRepositoryTest
    {
        private string TenantId { get; } = "my-tenant";

        private Mock<ITenantService> TenantService { get; set; }

        private IEventData InMemoryEventData
        {
            get
            {
                return new EventData
                {
                    Data = new { key = "key", value = "value" },
                    Name = "EventName",
                    Source = "Source",
                    Time = new Foundation.Date.TimeBucket(DateTimeOffset.Now)
                };
            }
        }

        private IEventStoreRepository GetRepository(IMongoConfiguration config)
        {
            TenantService = new Mock<ITenantService>();
            TenantService.Setup(s => s.Current).Returns(new TenantInfo() { Id = TenantId });
            return new EventStoreRepository
            (
                configuration: config,
                tenantService: TenantService.Object
            );
        }

        [MongoFact]
        public void InsertEventData_One()
        {
            MongoTest.Run((config) =>
            {
                // arrange
                var repo = GetRepository(config);

                // act
                repo.Add(InMemoryEventData);

                // assert
                var result = repo.All(x => x.TenantId == TenantId).Result;
                Assert.NotNull(result);
                Assert.NotEmpty(result);
                Assert.Equal(1, result.Count());
            });
        }

        [MongoFact]
        public void InsertEventData_Many()
        {
            MongoTest.Run((config) =>
            {
                // arrange
                var repo = GetRepository(config);

                // act
                repo.Add(InMemoryEventData);
                repo.Add(InMemoryEventData);
                repo.Add(InMemoryEventData);

                // assert
                var result = repo.All(x => x.TenantId == TenantId).Result;
                Assert.NotNull(result);
                Assert.NotEmpty(result);
                Assert.Equal(3, result.Count());
            });
        }
    }
}
