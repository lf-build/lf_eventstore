﻿using LendFoundry.EventHub;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Logging;
using LendFoundry.Security.Tokens;
using LendFoundry.Tenant.Client;
using Microsoft.AspNetCore.Builder;
using Moq;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace LendFoundry.EventStore.Tests
{
    public class EventStoreListenerTest
    {
        Mock<IEventHubClient> EventHub { get; } = new Mock<IEventHubClient>();

        Mock<IEventHubClientFactory> EventHubFactory { get; } = new Mock<IEventHubClientFactory>();

        Mock<ILogger> Logger { get; } = new Mock<ILogger>();

        Mock<ILoggerFactory> LoggerFactory { get; } = new Mock<ILoggerFactory>();

        Mock<IEventStoreRepositoryFactory> RepositoryFactory { get; } = new Mock<IEventStoreRepositoryFactory>();

        Mock<IEventStoreRepository> Repository { get; } = new Mock<IEventStoreRepository>();

        Mock<ITokenHandler> TokenHandler = new Mock<ITokenHandler>();

        Mock<ITenantServiceFactory> TenantServiceFactory { get; } = new Mock<ITenantServiceFactory>();

        Mock<ITenantService> TenantService { get; } = new Mock<ITenantService>();

        EventStoreListener Service
        {
            get
            {
                return new EventStoreListener
                (
                    RepositoryFactory.Object,
                    TokenHandler.Object,
                    LoggerFactory.Object,
                    EventHubFactory.Object,
                    TenantServiceFactory.Object
                );
            }
        }

        public EventStoreListenerTest()
        {
            Repository.Setup(s => s.Add(It.IsAny<IEventData>()));
            RepositoryFactory.Setup(s => s.Create(It.IsAny<ITokenReader>())).Returns(Repository.Object);

            Logger.Setup(s => s.Info(It.IsAny<string>()));
            Logger.Setup(s => s.Error(It.IsAny<string>(), It.IsAny<Exception>()));

            LoggerFactory.Setup(x => x.CreateLogger())
                .Returns(Logger.Object);

            EventHubFactory.Setup(x => x.Create(It.IsAny<ITokenReader>()))
                .Returns(EventHub.Object);

            //token

            IToken tokenRef = new Token { Value = "Value" };
            var token = new Mock<IToken>();
            token.Setup(x => x.Value).Returns("Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJteS1hcHAiLCJpYXQiOiIyMDE1LTEwLTA5VDA1OjExOjI3LjUzNzc3MjdaIiwiZXhwIjpudWxsLCJzdWIiOm51bGwsInRlbmFudCI6Im15LXRlbmFudCIsInNjb3BlIjpudWxsfQ.vVcomx0eqAs8WvFWEB-ntIB8kaijAqEzXB1AC8bNvaM");

            TokenHandler.Setup(x => x.Issue("9BFB3870-2075-4617-A893-3CA5F337B792", "event-store", null, "system", null))
            .Returns(new Token { Value = "Value" });
            
            TenantService.Setup(x => x.GetActiveTenants())
                .Returns(new List<TenantInfo>{
                    new TenantInfo
                        {
                            Id = "9BFB3870-2075-4617-A893-3CA5F337B792",
                            Name = "my-tenant",
                            Email = "mytenant@gmail.com",
                            Website = "www.tenant.com",
                            IsActive = true
                        }
                    });

            TenantServiceFactory.Setup(x => x.Create(It.IsAny<ITokenReader>()))
                .Returns(TenantService.Object);
        }

        [Fact]
        public void Initialize_WithNullParam_Throws_()
        {
            Assert.Throws<ArgumentException>(() =>
            {
                new EventStoreListener
                (
                      Mock.Of<IEventStoreRepositoryFactory>(),
                      Mock.Of<ITokenHandler>(),
                      null,
                      Mock.Of<IEventHubClientFactory>(),
                      Mock.Of<ITenantServiceFactory>()
                );
            });
        }

        [Fact]
        public void InitializeWhenAllDependenciesAreOk()
        {
            var instance = new EventStoreListener
            (
                Mock.Of<IEventStoreRepositoryFactory>(),
                Mock.Of<ITokenHandler>(),
                Mock.Of<ILoggerFactory>(),
                Mock.Of<IEventHubClientFactory>(),
                Mock.Of<ITenantServiceFactory>()
            );

            Assert.True(instance is IEventStoreListener);
        }

        [Fact]
        public void Start_WhenProcessOK()
        {
            // arrange        
            EventHub.Setup(s => s.On(It.IsAny<string>(), It.IsAny<Action<EventInfo>>()))
                    .Callback((string eventName, Action<EventInfo> handler) =>
                    {
                        // act
                        handler.Invoke(new EventInfo
                        {
                            Data = new { },
                            TenantId = "my-tenant",
                            Name = "Name",
                            Source = "Source",
                            Time = It.IsAny<TimeBucket>()
                        });

                        // assert
                        Logger.Verify(v => v.Info(It.IsAny<string>()), Times.AtLeastOnce);
                        RepositoryFactory.Verify(v => v.Create(It.IsAny<ITokenReader>()));
                        Repository.Verify(v => v.Add(It.IsAny<IEventData>()), Times.Once);
                    });

            // act
            Service.Start();

            // assert
            Logger.Verify(v => v.Info(It.IsAny<string>()), Times.AtLeastOnce);
            EventHub.Verify(v => v.On(It.IsAny<string>(), It.IsAny<Action<EventInfo>>()), Times.Once);
            EventHub.Verify(v => v.Start(), Times.Once);
        }

        [Fact]
        public void StartingEventStoreListenerExtensions()
        {
            Assert.Throws<InvalidOperationException>(() =>
            {
                var eventStoreListener = Mock.Of<IEventStoreListener>();
                eventStoreListener.Start();

                var applicationBuilder = new Mock<IApplicationBuilder>();
                var serviceProvider = new Mock<IServiceProvider>();

                serviceProvider.Setup(x => x.GetRequiredService<IEventStoreListener>())
                               .Returns(eventStoreListener);

                applicationBuilder.Setup(x => x.ApplicationServices)
                                  .Returns(serviceProvider.Object);

                EventStoreListenerExtensions.UseEventStoreListener(applicationBuilder.Object);
            });
        }
    }
    public interface IServiceProvider : System.IServiceProvider
    {
        T GetService<T>() where T : class;

        T GetRequiredService<T>() where T : class;
    }
}
