﻿using LendFoundry.EventHub.Client;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Logging;
using LendFoundry.Security.Tokens;
using LendFoundry.Tenant.Client;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace LendFoundry.EventStore.Tests
{
    public class EventStoreServiceTest
    {
        private EventStoreService service;

        private Mock<IEventStoreRepository> repository { get; }

        public EventStoreServiceTest()
        {
            repository = new Mock<IEventStoreRepository>();
            service = new EventStoreService(repository.Object);
        }
        
        [Fact]
        public void GetDataById_WithNullId_ThrowsArgumentException()
        {
            Assert.ThrowsAsync<ArgumentException>(async () =>
            {
                await service.GetDataById(null);
            });
        }

        [Fact]
        public async Task GetDataById_Returns_ResultAsync()
        {
            var response = new List<IEventData>() { { new EventData() { Name = "Event1" } } };
            repository
                .Setup(x => x.GetDataById(It.IsAny<string>()))
                .ReturnsAsync(response);

            var result = await service.GetDataById("Id1");

            Assert.Single(result);
            Assert.Equal("Event1", result.First().Name);
        }
    }
}

