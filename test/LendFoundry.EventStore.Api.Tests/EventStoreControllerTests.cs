﻿using LendFoundry.EventStore.Api.Controllers;
using LendFoundry.Foundation.Services;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.DependencyInjection;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xunit;

namespace LendFoundry.EventStore.Api.Tests
{
    public class EventStoreControllerTests
    {
        private EventStoreController EventStoreController { get; set; }
        private Mock<IEventStoreService> mockEventStoreService { get; set; }

        public EventStoreControllerTests()
        {
             mockEventStoreService = new Mock<IEventStoreService>();
            EventStoreController = new EventStoreController(mockEventStoreService.Object);
        }

        [Fact]
        public void EventStoreController_Constructor_ThrowsArgumentNullException()
        {
            Assert.Throws<ArgumentException>(() => new EventStoreController(null));
        }

        [Fact]
        public async void GetDataById_Returns_OkResult()
        {
            var response = new List<IEventData>() { new EventData() { Name = "Event1" } };

            mockEventStoreService.Setup(x => x.GetDataById(It.IsAny<string>())).ReturnsAsync(response);

            var result = await EventStoreController.GetDataByIdAsync(It.IsAny<string>());
            Assert.NotNull(result);
        }

        [Fact]
        public void GetDataById_Throws_ArgumentNullException()
        {
            mockEventStoreService.Setup(x => x.GetDataById(null)).ThrowsAsync(new ArgumentNullException());

            Assert.ThrowsAsync<ArgumentException>(async () =>
            {
                await EventStoreController.GetDataByIdAsync(null);
            });
        }
    }
}
